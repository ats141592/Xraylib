import matplotlib.pyplot
import numpy
import seaborn
import xraylib


if __name__=="__main__":
    seaborn.set        ()
    seaborn.set_context("poster")
    seaborn.set_palette("hls", 2)
    seaborn.set_style("ticks")
 
    energy                  = numpy.linspace(0e+00, 1e+06, 1000001) # keV
    total_cross_section     = numpy.zeros(len(energy))
    kl3line_cross_section   = numpy.zeros(len(energy))
    kl2line_cross_section   = numpy.zeros(len(energy))

    for i in range(len(energy)):
        try:
            total_cross_section[i]      = xraylib.CSb_Photo_Total(Z=26, E=energy[i]/1000)
        except:
            total_cross_section[i]      = 0
        
        try:
            kl3line_cross_section[i]    = xraylib.CSb_FluorLine_Kissel(Z=26, line=xraylib.KL3_LINE, E=energy[i]/1000)
        except:
            kl3line_cross_section[i]    = 0
        
        try:
            kl2line_cross_section[i]    = xraylib.CSb_FluorLine_Kissel(Z=26, line=xraylib.KL2_LINE, E=energy[i]/1000)
        except:
            kl2line_cross_section[i]    = 0

    fig = matplotlib.pyplot.figure  (dpi=200, figsize=(16, 9))
    fig.subplots_adjust             (left=0.150, bottom=0.125, right=0.95, top=0.95)
    matplotlib.pyplot.axis          ([1.00e+01, 1.00e+06, +0.00e+00, +1.00e+00])
    matplotlib.pyplot.plot          (energy, kl3line_cross_section/total_cross_section, label="Geant4", linewidth=1, marker=".", markersize=4)
    matplotlib.pyplot.legend        (loc="upper right")
    matplotlib.pyplot.tick_params   (labelsize=30)
    matplotlib.pyplot.xlabel        ("Energy (eV)", fontsize=30)
    matplotlib.pyplot.xscale        ("log")
    matplotlib.pyplot.ylabel        ("Yield", fontsize=30)
    matplotlib.pyplot.yscale        ("linear")
    matplotlib.pyplot.savefig       ("../figure/2602.png")
    matplotlib.pyplot.close         ()