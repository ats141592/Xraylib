import matplotlib.pyplot
import numpy
import seaborn
import tqdm


if __name__=="__main__":
    seaborn.set        ()
    seaborn.set_context("poster")
    seaborn.set_palette("hls", 2)
    seaborn.set_style("ticks")
    
    for i in tqdm.tqdm(range(1, 31)):
        for j in range(5):
            if j==0:
                fin01   = numpy.loadtxt("/Users/tanimoto/software/monaco/database/phot/pe-cs-{0:d}.dat".format(i))
                fin02   = numpy.loadtxt("../database/pe-cs-{0:d}.dat".format(i))
            elif j==1:
                fin01   = numpy.loadtxt("/Users/tanimoto/software/monaco/database/phot/pe-ss-cs-{0:d}-K.dat".format(i))
                fin02   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-K.dat".format(i))
            elif j==2:
                fin01   = numpy.loadtxt("/Users/tanimoto/software/monaco/database/phot/pe-ss-cs-{0:d}-L1.dat".format(i))
                fin02   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-L1.dat".format(i))
            elif j==3:
                fin01   = numpy.loadtxt("/Users/tanimoto/software/monaco/database/phot/pe-ss-cs-{0:d}-L2.dat".format(i))
                fin02   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-L2.dat".format(i))
            elif j==4:
                fin01   = numpy.loadtxt("/Users/tanimoto/software/monaco/database/phot/pe-ss-cs-{0:d}-L3.dat".format(i))
                fin02   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-L3.dat".format(i))
            
            energy01    = numpy.zeros(len(fin01)-2)
            energy02    = numpy.zeros(len(fin02)-2)
            cross01     = numpy.zeros(len(fin01)-2)
            cross02     = numpy.zeros(len(fin02)-2)

            for k in range(len(fin01)-2):
                energy01[k] = 1.00e+03*fin01[k][0]
                cross01[k]  = 1.00e+00*fin01[k][1]
    
            for k in range(len(fin02)-2):
                energy02[k] = 1.00e+03*fin02[k][0]
                cross02[k]  = 1.00e+00*fin02[k][1]

            fig = matplotlib.pyplot.figure  (dpi=200, figsize=(16, 9))
            fig.subplots_adjust             (left=0.150, bottom=0.125, right=0.95, top=0.95)
            matplotlib.pyplot.axis          ([1.00e-02, 1.00e+03, +1.00e+00, +1.00e+08])
            matplotlib.pyplot.plot          (energy01, cross01, label="Geant4", linewidth=1, marker=".", markersize=4)
            matplotlib.pyplot.plot          (energy02, cross02, label="Xraylib", linewidth=1, marker=".", markersize=4)
            matplotlib.pyplot.legend        (loc="upper right")
            matplotlib.pyplot.tick_params   (labelsize=30)
            matplotlib.pyplot.xlabel        ("Energy (keV)", fontsize=30)
            matplotlib.pyplot.xscale        ("log")
            matplotlib.pyplot.ylabel        ("Cross Section (barn/atom)", fontsize=30)
            matplotlib.pyplot.yscale        ("log")
            matplotlib.pyplot.savefig       ("../figure/{0:02d}{1:02d}.png".format(i, j+2))
            matplotlib.pyplot.close         ()