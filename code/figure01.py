import matplotlib.pyplot
import numpy
import seaborn
import tqdm


if __name__=="__main__":
    seaborn.set        ()
    seaborn.set_context("poster")
    seaborn.set_palette("hls", 5)
    seaborn.set_style("ticks")
    
    for i in tqdm.tqdm(range(1, 31)):
        fin01   = numpy.loadtxt("../database/pe-cs-{0:d}.dat".format(i))
        fin02   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-K.dat".format(i))
        fin03   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-L1.dat".format(i))
        fin04   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-L2.dat".format(i))
        fin05   = numpy.loadtxt("../database/pe-ss-cs-{0:d}-L3.dat".format(i))
        energy  = numpy.zeros(len(fin01)-2)
        cross01 = numpy.zeros(len(fin01)-2)
        cross02 = numpy.zeros(len(fin01)-2)
        cross03 = numpy.zeros(len(fin01)-2)
        cross04 = numpy.zeros(len(fin01)-2)
        cross05 = numpy.zeros(len(fin01)-2)
    
        for j in range(len(fin01)-2):
            energy[j]   = 1.00e+06*fin01[j][0]
            cross01[j]  = 1.00e+00*fin01[j][1]
            cross02[j]  = 1.00e+00*fin02[j][1]
            cross03[j]  = 1.00e+00*fin03[j][1]
            cross04[j]  = 1.00e+00*fin04[j][1]
            cross05[j]  = 1.00e+00*fin05[j][1]

        fig = matplotlib.pyplot.figure  (dpi=200, figsize=(16, 9))
        fig.subplots_adjust             (left=0.150, bottom=0.125, right=0.95, top=0.95)
        matplotlib.pyplot.axis          ([1.00e+01, 1.00e+06, +1.00e+00, +1.00e+08])
        matplotlib.pyplot.plot          (energy, cross01, label="Total", linewidth=1, marker=".", markersize=4)
        matplotlib.pyplot.plot          (energy, cross02, label="K-shell", linewidth=1, marker=".", markersize=4)
        matplotlib.pyplot.plot          (energy, cross03, label="L1-shell", linewidth=1, marker=".", markersize=4)
        matplotlib.pyplot.plot          (energy, cross04, label="L2-shell", linewidth=1, marker=".", markersize=4)
        matplotlib.pyplot.plot          (energy, cross05, label="L3-shell", linewidth=1, marker=".", markersize=4)
        matplotlib.pyplot.legend        (loc="upper right")
        matplotlib.pyplot.tick_params   (labelsize=30)
        matplotlib.pyplot.xlabel        ("Energy (eV)", fontsize=30)
        matplotlib.pyplot.xscale        ("log")
        matplotlib.pyplot.ylabel        ("Cross Section (barn/atom)", fontsize=30)
        matplotlib.pyplot.yscale        ("log")
        matplotlib.pyplot.savefig       ("../figure/{0:02d}01.png".format(i))
        matplotlib.pyplot.close         ()