import numpy
import tqdm
import xraylib


def GetTotalCrossSection(atomic_number, energy_array):
    cross_array = numpy.zeros(len(energy_array))
    
    for i in range(len(energy_array)):
        try:
            cross_array[i] = xraylib.CSb_Photo_Total(atomic_number, energy_array[i]/1000)
        except:
            cross_array[i] = 0

    return cross_array


def GetShellCrossSection(atomic_number, shell, energy_array):
    cross_array = numpy.zeros(len(energy_array))

    for i in range(len(energy_array)):
        try:
            cross_array[i] = xraylib.ElectronConfig(atomic_number, shell)*xraylib.CSb_Photo_Partial(atomic_number, shell, energy_array[i]/1000)
        except:
            cross_array[i] = 0

    return cross_array


def WriteCrossSection(atomic_number, shell, energy_array, cross_array):
    if shell==-1:
        filename = "../database/pe-cs-{}.dat".format(atomic_number)
    elif shell==xraylib.K_SHELL:
        filename = "../database/pe-ss-cs-{}-K.dat".format(atomic_number)
    elif shell==xraylib.L1_SHELL:
        filename = "../database/pe-ss-cs-{}-L1.dat".format(atomic_number)
    elif shell==xraylib.L2_SHELL:
        filename = "../database/pe-ss-cs-{}-L2.dat".format(atomic_number)
    elif shell==xraylib.L3_SHELL:
        filename = "../database/pe-ss-cs-{}-L3.dat".format(atomic_number)

    with open(filename, mode="w") as fout:
        for i in range(len(cross_array)):
            fout.write("{0:.8e} {1:.8e}\n".format(1.00e-06*energy_array[i], cross_array[i]))
        else:
            fout.write("-1 -1\n")
            fout.write("-2 -2\n")


if __name__=="__main__":
    energy_array            = numpy.zeros(20981)
    
    for i in range(len(energy_array)):
        if i==0:
            pass
        elif i<=20000:
            energy_array[i] = energy_array[i-1]+1
        else:
            energy_array[i] = energy_array[i-1]+1000
    
    for atomic_number in tqdm.tqdm(range(1, 31)):
        cross_total_array   = GetTotalCrossSection(atomic_number, energy_array)
        cross_kshell_array  = GetShellCrossSection(atomic_number, xraylib.K_SHELL, energy_array)
        cross_l1shell_array = GetShellCrossSection(atomic_number, xraylib.L1_SHELL, energy_array)
        cross_l2shell_array = GetShellCrossSection(atomic_number, xraylib.L2_SHELL, energy_array)
        cross_l3shell_array = GetShellCrossSection(atomic_number, xraylib.L3_SHELL, energy_array)
        WriteCrossSection(atomic_number, -1, energy_array, cross_total_array)
        WriteCrossSection(atomic_number, xraylib.K_SHELL, energy_array, cross_kshell_array)
        WriteCrossSection(atomic_number, xraylib.L1_SHELL, energy_array, cross_l1shell_array)
        WriteCrossSection(atomic_number, xraylib.L2_SHELL, energy_array, cross_l2shell_array)
        WriteCrossSection(atomic_number, xraylib.L3_SHELL, energy_array, cross_l3shell_array)
